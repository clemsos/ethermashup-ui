/* @pjs font="/pjs/MiscFixedSC613-13.ttf"; crisp=true;*/
/*
(0,0)->lat:83  lon:-20
(width,height) ->lat:-53  lon:-20
*/

PImage carte; 
ArrayList<Line> connex;

void setup(){
	size(window.innerWidth, window.innerHeight);

	carte = loadImage("/img/fond-carte_china2.png");
	imageMode(CENTER);
	rectMode(CENTER);
	smooth();

	textFont(createFont("/pjs/MiscFixedSC613-13.ttf",int(width/40)));
	connex = new ArrayList<Line>();
}

void draw(){
	background(0);

	if(carte.width>0) image(carte,width/2,height/2,width,(width/carte.width)*carte.height);

	for (Line l : connex){
		l.update();
		l.draw();
	}
}

void keyPressed(){
	if (key=='+') taille++;
	else if (key=='-') taille--;
}

void mousePressed(){
	println("taille: "+taille);	
}

void addConnection(float x1, float y1, float x2, float y2, string _in, string _out){
	connex.add(new Line(x1, y1, x2, y2, _in, _out));
}

