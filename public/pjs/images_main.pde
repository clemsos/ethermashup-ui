/* @pjs font="/pjs/MiscFixedSC613-13.ttf"; crisp=true;*/

ArrayList<Img> imgs;
String[] urls;
int simul = 20;
int nb=0;
int nMax;
boolean play = true;
PFont f1;

void setup(){
	size(window.innerWidth, window.innerHeight);
	imageMode(CENTER);
	rectMode(CENTER);
	background(0);
	frameRate(15);

	urls = loadStrings("/data/urls.txt");
	nMax = urls.length();
	imgs = new ArrayList<Img>();
	for (int i = 0; i<simul; i++){
		imgs.add(new Img("/data/"+urls[nb]));
		nb++;
	}
	f1 = createFont("/pjs/MiscFixedSC613-13.ttf",int(width/40));
	textFont(f1);
}

void draw(){
	if(play){//delais
		for (int i = imgs.size()-1; i>= 0; i-- ){
			Img im = imgs.get(i);
			im.draw();
			if(im.displayed){
				imgs.remove(i);
				imgs.add(new Img("/data/"+urls[nb]));
				nb++;

				// console.log(nb);

				if(nb>=nMax) nb=0;
				i=-1;

				play=false;
				setTimeout(function(){play=true;},random((random(1)<.8?50:1500)));
			}
		}
	}
}

class Img{
	String url;
	boolean displayed = false;
	PImage img;
	String ipIn;
	String ipOut;

	Img(String _url){
		url = _url;
		img = loadImage(url);
		ipIn = "FROM: "+int(random(256))+"."+int(random(256))+"."+int(random(256))+"."+int(random(256));
		ipOut = "TO: "+int(random(256))+"."+int(random(256))+"."+int(random(256))+"."+int(random(256));
       // println(ip);
	}

	void draw(){
		if(img.width>0){
			image(img,width/2, height/2, width, height);
			// image(img,width/2,height/2);

			if(random(1)<.1) {
				fill(0);
				rect(width/2,height/2,textWidth(url)+30,int(width/40)+30);
				fill(255);
				textAlign(CENTER);
				text(url,width/2,height/2+15);
			}

			fill(0,255,0);
			textAlign(LEFT);
			text(ipIn,15,5+width/40);

			fill(255,0,0);
			textAlign(RIGHT);
			text(ipOut,width-15,5+width/40);

			displayed = true;
		}
	}
}