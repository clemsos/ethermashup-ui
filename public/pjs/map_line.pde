class Line{
	PVector from, to;
	PVector pos = new PVector(-100,-100); //lerp position
	float p = 0; // var going from 0.0 to 1.0
	float speed = 5; //speed
	float trait = 2;
	int c = 180;
	boolean fade = false;
	int taille = 8;
	String ipIn;
	String ipOut;

	Line(float x1, float y1, float x2, float y2, string _in, string _out){
		/*
		(0,0)->lat:83  lon:-20
		(width,height) ->lat:-53  lon:-20
		*/
		x1 = (x1>=-20) ? map(x1,-20,340, 0,width) : map(340+x1,-20,340, 0,width);
		x1 = int(x1/taille)*taille;
		y1 = map(y1,83,-53,0,height);
		y1 = int(y1/taille)*taille;
		from = new PVector(x1,y1);
		x2 = (x2>=-20) ? map(x2,-20,340, 0,width) : map(340+x2,-20,340, 0,width);
		x2 = int(x2/taille)*taille;
		y2 = map(y2,83,-53,0,height);
		y2 = int(y2/taille)*taille;
		to = new PVector(x2,y2);

		// ipIn = "FROM: "+int(random(256))+"."+int(random(256))+"."+int(random(256))+"."+int(random(256));
		// ipOut = "TO: "+int(random(256))+"."+int(random(256))+"."+int(random(256))+"."+int(random(256));
		ipIn = _in;
		ipOut = _out;

	}

	void update(){
		if(p<100){
			p+=speed;
		}
		else p = 100;
		pos = PVecLerp(from,to,p/100.);

		if(c>0){
			c -= 0.9;
			c=max(0,c);
		}
	}

	void draw(){
		if(c>=0){
			stroke(0,180,0,c);
			strokeWeight(trait);
			PVecLine(from,pos);
		}

		noStroke();

		fill(0,0,0);
		rect(0,width/40,width*2,50);
		// rect(width-100,5+width/40,400,15);

		fill(0,255,0);
		textAlign(LEFT);
		text(ipIn,15,5+width/40);
		PVecRect(from,taille-2,taille-2);
		

		fill(255,0,0);
		textAlign(RIGHT);
		text(ipOut,width-15,5+width/40);
		PVecRect(to,taille-2,taille-2);


	}

};
