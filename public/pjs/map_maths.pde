/*
 *     __  ___      __  __         ______                 __  _                 
 *    /  |/  /___ _/ /_/ /_  _____/ ____/_  ______  _____/ /_(_)___  ____  _____
 *   / /|_/ / __ `/ __/ __ \/ ___/ /_  / / / / __ \/ ___/ __/ / __ \/ __ \/ ___/
 *  / /  / / /_/ / /_/ / / (__  ) __/ / /_/ / / / / /__/ /_/ / /_/ / / / (__  ) 
 * /_/  /_/\__,_/\__/_/ /_/____/_/    \__,_/_/ /_/\___/\__/_/\____/_/ /_/____/  
 */
// Infinite Line Intersection2
PVector lineIntersection(PVector a, PVector b, PVector c, PVector d) {
	float bx = b.x - a.x;
	float by = b.y - a.y;
	float dx = d.x - c.x;
	float dy = d.y - c.y; 
	float b_dot_d_perp = bx*dy - by*dx;
	if (b_dot_d_perp == 0) {
		return null;
	}
	float cx = c.x-a.x; 
	float cy = c.y-a.y;
	float t = (cx*dy - cy*dx) / b_dot_d_perp; 

	return new PVector(a.x+t*bx, a.y+t*by);
}

// Line Segment Intersection
PVector segIntersection(PVector a, PVector b, PVector c, PVector d) { 
	float bx = b.x - a.x;
	float by = b.y - a.y;
	float dx = d.x - c.x;
	float dy = d.y - c.y;
	float b_dot_d_perp = bx * dy - by * dx;
	if (b_dot_d_perp == 0) {
		return null;
	}
	float cx = c.x - a.x;
	float cy = c.y - a.y;
	float t = (cx * dy - cy * dx) / b_dot_d_perp;
	if (t < 0 || t > 1) {
		return null;
	}
	float u = (cx * by - cy * bx) / b_dot_d_perp;
	if (u < 0 || u > 1) { 
		return null;
	}
	return new PVector(a.x+t*bx, a.y+t*by);
}

// Point in Polygon
boolean pointInPolygon(PVector point, PVector[] vs) {
	// ray-casting algorithm based on
	// http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html

	float x = point.x, y = point.y;

	boolean inside = false;
	for (int i = 0, j = vs.length - 1; i < vs.length; j = i++) {
		float xi = vs[i].x, yi = vs[i].y;
		float xj = vs[j].x, yj = vs[j].y;

		boolean intersect = ((yi > y) != (yj > y)) && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
		if (intersect) inside = !inside;
	}

	return inside;
}

float ease(float variable, float target, float easingVal) {
	float d = target - variable;
	if (abs(d)>1) variable+= d*easingVal;
	return variable;
}

PVector PVecLerp(PVector v1, PVector v2, float amt) {
	PVector v = new PVector(lerp(v1.x, v2.x, amt),lerp(v1.y, v2.y, amt));
	return v;
}

void PVecLine(PVector a, PVector b) {
	line(a.x, a.y, b.x, b.y);
}

void PVecPoint(PVector a) {
	point(a.x, a.y);
}

void PVecRect(PVector a, float w, float h) {
	rect(a.x, a.y, w, h);
}