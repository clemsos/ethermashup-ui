// Packet.js is the main class for server/client communication through web sockets

var Packet = function() {

    var self = this;

    /*
        MODEL

        self.from       : Boolean
        self.geo        : String
        self.images     : Array [String]
        self.text       : String
        // self.diff       : Int (not implemented)
    */

    self.images=[];

    self.setFrom = function(from) {
      if(typeof(from) != boolean )
        throw new Error('Invalid type from: ' + from);
      self.from = from;  
    }
    
    self.setText = function(text) {
      if(typeof(text) != string )
        throw new Error('Invalid type text: ' + text);
      self.text = text;  
    }

    self.setGeo = function(geo) {
      if(typeof(geo) != string )
        throw new Error('Invalid type geo: ' + geo);
      self.geo = geo;  
    }

    self.addImage = function(image) {
      if(typeof(image) != string )
        throw new Error('Invalid type image: ' + image);
      self.images.push(image);  
    }

    self.toJSON = function() {
      return JSON.stringify({
        from: self.from,
        geo: self.geo,
        img: self.img,
        text: self.text
      })
    }

}