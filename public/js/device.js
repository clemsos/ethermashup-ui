///////// VARIABLES
	var nbJson=20;
	var DUMPS_PATH="../dump/";
	var filebase="dump";
	var SERVER_IP="103.5.12.91";
	var isConnectedToServer = false;

/////////GET DATA FROM SOCKET
	var devicesocket = io.connect();
	devicesocket.on('connect', function (data) {

		// autoAdd=false;
		console.log(data);

		// get mac address from URL
		var sp=String(document.location).split('/');
		var mac=sp[sp.length-1]

		devicesocket.emit('device',{"mac":mac});
	});

	devicesocket.on('packet', function (data) {
		// console.log("data", data);
		addMessage(JSON.parse(data));
	});

///////// ADD MESSAGES
	function initMessage(){
		console.log('initMessage');
		$('#content').append("<p class='blink'>|</p>");
	}

	// var images;
	function addMessage(jsonObject){
		$("p.blink").remove();

		var from = jsonObject.from;
		var geo = jsonObject.geo;
		var images = jsonObject.images;
		var text = jsonObject.text;

		var div = $("<div class='message'></div>");
		$('#content').append(div);
		var t = 0;

		//From - To
		div.append("<p class='fromTo'>FROM " + geo.toUpperCase() + "</p>");
		string2Sound(geo.toUpperCase(),2);
		/*if(from){
			div.append("<p class='fromTo'>FROM " + geo.toUpperCase() + "</p>");
		}
		else{
			div.append("<p class='fromTo'>TO " + geo.toUpperCase() + "</p>");
		}*/

		//text
		if(text != '...'){
			// console.log(splitTokens(text,'{}[]<=%/\\')[0]);
			var spli = splitTokens(text,'{}[|]><=%/\\');
			text = spli[0];

			t = Math.floor(Math.random()*600);
			setTimeout(function(){
				div.append("<p>" + text + (spli.length>1 ? "..." : "") + "</p>");
				string2Sound(text,1);
			}, t);
		}

		// images
		var iteratorImages = 0;
		if(images.length>0){
			t += Math.floor(Math.random()*(1400 - t));
			setTimeout(addImage,t);
		}
		else setTimeout(addCaret, t+100);

		function addImage(){
			console.log('iteratorImages: '+ iteratorImages);
			
			// get image
			$.get(images[iteratorImages],function(data){
				var img = new Image();
				img.src = data;
				if(img.width>10){
					if(iteratorImages == 0) div.append("<br>");
					div.append(img);
					string2Sound("image"+Math.random()*10000,3);
				}

				var _t = Math.floor(Math.random()*(1400 - t));
				t += _t;
				iteratorImages++;
				if(iteratorImages<images.length) setTimeout(addImage, _t);
				else setTimeout(addCaret, t+100);
			})

		}

		function addCaret(){
			div.append("<p class='blink'>|</p>");
		}

		// console.log('removing outer messages');
		while($('#content').height()>window.innerHeight*1.2){
			$('.message').first().remove();
			// console.log($('#content').height(), window.innerHeight);
		}
	}

/////////BLINKING CARET
	function blink(){
		var $blink;
		setInterval(function(){
				$blink = $('.blink').last();
				if( $blink.css('visibility')=='hidden' ){
						$blink.css('visibility','visible');
				}else{
						$blink.css('visibility','hidden');
				}
		},500);
	}

/////////AUDIO
	var audio_context, oscillator, source, gainNode, filter;
	var freq;

	function initAudio(){
		console.log('initAudio');
		try {
			audio_context = new (window.AudioContext || window.webkitAudioContext);
			oscillator = audio_context.createOscillator();
					filter = context.createBiquadFilter();
		} catch (e) {
			
		}
	}

	function string2Sound(str, type) {
		freq = hex2Dec(string2Hex(str))/30 + (type===2?19000:type===3?19000:19000);
		console.log('freq: '+ freq);
		play(freq, type);
		setTimeout(stop, (type===3?0:type===1?0:0));
	}

	function stop() {
		oscillator.noteOff(0);
		oscillator = audio_context.createOscillator();
	}

	function play(freq, type) {
		oscillator.frequency.value = freq;
		oscillator.type = (type===1?1:type===2?2:3);
		oscillator.connect(audio_context.destination);
		oscillator.noteOn(0);
	}

/////////AUTO ADD MESSAGE FROM JSONS
	/*
	var jsons = [];
	var iterator = 0;
	var activeJSon = 0;
	function initAutoAddMessage(){
		console.log('initAutoAddMessage');
		for (var i = 0; i < nbJson; i++) {
			console.log(i);
			$.ajax({
				type: 'GET',
				url: DUMPS_PATH+filebase+i+'.json',
				dataType: 'json',
				success: function(data) {
					jsons.push(data);
					if(jsons.length == nbJson){
						// console.log(jsons);
						// autoAdd = true;
					}
				},
				data: {},
				async: false
			});
		};
	}
	
	function autoAddMessage(){
		if(isConnectedToServer){
		// if(autoAdd){
			// var activeJSon = Math.floor(iterator/10);
			if(iterator < jsons[activeJSon].length-1){
				// console.log('jsons[activeJSon].length: '+ jsons[activeJSon].length);
				iterator++;
				// console.log('iterator: '+ iterator);
			}
			else{
				iterator = 0;
				activeJSon = (activeJSon+1) % 20;
				// console.log('activeJSon: ' + activeJSon);
				iterator = 0;
			}
			addMessage(jsons[activeJSon][iterator]);
		}
		setTimeout(autoAddMessage, 1500+Math.floor(Math.random()>.8?5000+Math.random()*8000:Math.random()*5000));
	}
	autoAddMessage();
	*/

///////// ONLOAD
	$(window).ready(function(){
		var device = $('#device').text() + document.location.href[document.location.href.length-1];
		console.log('device: '+ device);
		$('#device').text(device).append("<br>-");
		initMessage();
		blink();
		initAudio();
		// initAutoAddMessage();
	});

////////SplitTokens function taken from Processing.js
	function splitTokens(str, tokens) {
		var chars = tokens.split(/()/g),
			buffer = "",
			len = str.length,
			i, c,
			tokenized = [];

		for (i = 0; i < len; i++) {
			c = str[i];
			if (chars.indexOf(c) > -1) {
				if (buffer !== "") {
					tokenized.push(buffer);
				}
				buffer = "";
			}
			else {
				buffer += c;
			}
		}

		if (buffer !== "") {
			tokenized.push(buffer);
		}

		return tokenized;
	}

/////////CONVERSION UTILS
	// from http://snipplr.com/view.php?codeview&id=52975
	function dec2Hex(d) {
		return d.toString(16);
	}

	function hex2Dec (h) {
		return parseInt(h, 16);
	}

	function string2Hex (tmp) {
		var str = '',
			i = 0,
			tmp_len = tmp.length,
			c;
		
		for (; i < tmp_len; i++) {
			c = tmp.charCodeAt(i);
			str += dec2Hex(c) + ' ';
		}
		return str;
	}

	function hex2String (tmp) {
		var arr = tmp.split(' '),
			str = '',
			i = 0,
			arr_len = arr.length,
			c;
		
		for (; i < arr_len; i++) {
			c = String.fromCharCode( hex2Dec( arr[i] ) );
			str += c;
		}
		
		return str;
	}

	function string2Bin(str){
		var st,i,j,d;
		var arr = [];
		var len = str.length;
		for (i = 1; i<=len; i++){
			//reverse so its like a stack
			d = str.charCodeAt(len-i);
			for (j = 0; j < 8; j++) {
				arr.push(d%2);
				d = Math.floor(d/2);
			}
		}
		//reverse all bits again.
		return arr.reverse().join("");
	}
