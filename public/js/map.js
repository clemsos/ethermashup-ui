var mapsocket = io.connect();

mapsocket.on('connect', function (data) {
	console.log(data);
});

mapsocket.on('geoslice', function (data) {
	var myObject = eval('(' + data + ')');
	console.log(myObject);
 
    var cityIn = myObject.ipIn + " " + myObject.geoFrom.city +" "+ myObject.geoFrom.country;
    var cityOut = myObject.ipOut + " " + myObject.geoTo.city +" "+ myObject.geoTo.country;

	var pjs = Processing.getInstanceById('sketch');
	pjs.addConnection(myObject.fromLat, myObject.fromLong, myObject.toLat, myObject.toLong, cityIn, cityOut);
    
});