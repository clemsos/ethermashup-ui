require 'capistrano/ext/multistage'

##### Load server-specific variables
set :stages, ["staging", "production"]
set :default_stage, "production"


set :application, "ethermashup" 
set :scm, :git
set :repository, "git@bitbucket.org:clemsos/ethermashup-ui.git"
set :scm_passphrase, ""


set :node_file, "start.sh"
set :application_binary, 'bash'
set :deploy_via, :remote_cache

# role :app, :host
set :use_sudo, false
default_run_options[:pty] = true
set :ssh_options, {:forward_agent => true}

desc "Echo the server's hostname"
task :echo_hostname do 
  run "echo `hostname`"
end

namespace :deploy do

  # before 'deploy:node_additional_setup', 'deploy:setup_npm'
  after 'deploy:setup', 'deploy:node_additional_setup'

  before 'deploy:start', 'deploy:npm_install'
  before 'deploy:restart', 'deploy:npm_install'
  after 'deploy:create_symlink', 'deploy:symlink_node_folders'
  after 'deploy:create_symlink', 'deploy:symlink_config_files'
  # before 'deploy:restart', 'deploy:update_viz'

  before 'deploy:setup', 'deploy:create_deploy_to_with_sudo'
  after 'deploy:setup', 'deploy:write_upstart_script'
  
  
  task :start, :roles => :app, :except => { :no_release => true } do
    # run "#{try_sudo :as => 'root'} start #{application}"
    run "#{try_sudo :as => 'root'} start #{application}"
  end

  
  desc 'Install the current npm environment.'
  task :setup_npm, :roles => :app do
    invoke_command "bash -c '. ~/nvm/nvm.sh && cd #{current_path} && npm install'", :via => run_method
  end


  task :stop, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo :as => 'root'} stop #{application}"
  end

  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo :as => 'root'} restart #{application}"
  end

  task :create_deploy_to_with_sudo, :roles => :app do
    run "#{try_sudo :as => 'root'} mkdir -p #{deploy_to}"
    run "#{try_sudo :as => 'root'} chown #{admin_runner}:#{admin_runner} #{deploy_to}"
  end

  desc 'Create symlink between static npm modules and current path.'
  task :symlink_node_folders, :roles => :app, :except => { :no_release => true } do
    run "ln -s #{applicationdir}/shared/node_modules #{applicationdir}/current/node_modules"
    run "ln -s #{applicationdir}/data #{applicationdir}/current/public/data"
  end

 task :symlink_config_files do
    # run "ln -s #{shared_path}/config/db.json #{release_path}/config/"
    run "ln -s #{shared_path}/config/config.json #{release_path}/config/"

  end

  
  task :node_additional_setup, :roles => :app, :except => { :no_release => true } do
    # run "mkdir -p #{applicationdir}/shared/node_modules"
    invoke_command "bash -c '. ~/nvm/nvm.sh && cd #{current_path} && npm install forever -g'", :via => run_method
    invoke_command "bash -c '. ~/nvm/nvm.sh && cd #{current_path} && npm install supervisor -g'", :via => run_method
    # run "npm install forever -g"
    # run "npm install supervisor -g"
  end
 
  desc 'Install all npm packages'
  task :npm_install, :roles => :app, :except => { :no_release => true } do
    invoke_command "bash -c '. ~/nvm/nvm.sh && cd #{current_path} && npm install'", :via => run_method
  end

  desc 'Update all npm packages'
  task :npm_update, :roles => :app, :except => { :no_release => true } do
    run "cd #{applicationdir}/current/ #{try_sudo :as => 'root'} npm update"
  end

  # task :update_viz, :roles => :app, :except => { :no_release => true } do
  #   run "cd #{shared_path}/submodules/seuron_viz && git pull origin master"
  #   run "ln -s #{shared_path}/submodules/seuron_viz #{release_path}/public/viz/"
  # end

  desc 'Write the bash script in /etc/init.d with upstart'
  task :write_upstart_script, :roles => :app do
    upstart_script = <<-UPSTART
  description "#{application}"

  start on startup
  stop on shutdown

  script
      # We found $HOME is needed. Without it, we ran into problems
      env HOME="/home/#{admin_runner}"
      export HOME
      cd #{current_path}

      #{application_binary} #{current_path}/#{node_file} >> #{shared_path}/log/#{application}.log 2>&1
  end script
  respawn
UPSTART
  put upstart_script, "/tmp/#{application}_upstart.conf"
    sudo "mv /tmp/#{application}_upstart.conf /etc/init/#{application}.conf"
  end
end
