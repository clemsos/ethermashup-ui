// Packet.js is the main class for server/client communication through web sockets

var Packet = function() {

    var self = this;

    /*
        MODEL

        self.from       : Boolean
        self.geo        : String
        self.images     : Array [String]
        self.text       : String
        self.mac
        // self.diff       : Int (not implemented)
    */

    self.images=[];
    self.imagesPath=[];

    self.setFrom = function(from) {
      // if(typeof(from) != boolean )
        // throw new Error('Invalid type from: ' + from);
      self.from = from;  
    }
    
    self.setText = function(text) {
      if(typeof(text) != "string" )
        throw new Error('Invalid type text: ' + text);
      self.text = text;  
    }

    self.setGeo = function(geo) {
      if(typeof(geo) != "string" )
        throw new Error('Invalid type geo: ' + geo);
      self.geo = geo;  
    }

    self.setMac = function(mac) {
      if(typeof(mac) != "string" )
        throw new Error('Invalid type mac: ' + mac);
      self.mac = mac;  
    }

    self.addImage = function(image) {
      // if(typeof(image) != "string" )
        // throw new Error('Invalid type image: ' + image);
      self.images.push(image);  
    }

    
    self.addImagePath= function(path) {
      self.imagesPath.push(path)
    }

    self.populateFromMongo=function(mongoData) {
        self.from= mongoData.from;
        self.geo= mongoData.geo;
        var imgpath;
        for (var i = 0; i < mongoData.images.length; i++) {
          imgpath="/images/"+mongoData._id+"/"+i
          self.addImagePath(imgpath);
        };
        self.text= mongoData.text;
        self.mac= mongoData.mac;
    }

    self.toSocketJSON = function() {
      return JSON.stringify({
        from: self.from,
        geo: self.geo,
        images: self.imagesPath,
        text: self.text,
        mac: self.mac
      });
    }

    self.toJSON = function() {
      return JSON.stringify({
        from: self.from,
        geo: self.geo,
        images: self.images,
        text: self.text,
        mac: self.mac
      });
    }

}

module.exports.Packet = Packet;