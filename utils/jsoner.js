var data = require("../dump/dump.json");
var fs = require("fs");

var basedir = "../dump/";
// var nbPacketsByJson = 25;
// var nbJson = Math.ceil(data.length / nbPacketsByJson);
var nbDevices=6;

// var counter = 0;
var jsons=[];
// var jsons=new Array(6);

// create arrays to store data
for (var i = 0; i < nbDevices; i++) {
	jsons[i]=[];
};
console.log(jsons)

// parse data in arrays, order by devices
for (var j = 0; j<data.length; j++){
	
	var z=data[j].mac[data[j].mac.length-1]-1;
	// console.log(data[j].mac,z);
	jsons[z].push(data[j])
};

// split into files
for (var j = 0; j < jsons.length; j++) {

	var current=jsons[j];
	var filename = basedir + "dump_device" + j + ".json";
	console.log(filename, current.length);
	var json = "[";
	for (var k = 0; k < jsons[j].length; k++) {
		if(k==jsons[j].length-1) json += JSON.stringify(current[k]);
		else json += JSON.stringify(current[k])+',';
	};
	json += "]";
	
	fs.writeFile(filename, json, function(err) {
		if(err) {
			console.log(err);
		} else {
			console.log("The file was saved!");
		}
	});
};