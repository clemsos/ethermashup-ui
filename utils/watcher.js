var moment = require("moment");
var walk = require('walk');
var fs = require('fs');
var easyimg = require('imagemagick');

var allowedExtension = ['html', 'jpg', 'jpeg', 'png', 'gif'];

/* Main function :
	check folders timediff
	check files extension
	push to redis raw queue
*/

// find directories
function search4Folders(_path, callback){
	
	// Walker options
	var walker = walk.walk(_path, { followLinks: true });
	var folders = [];

	walker.on('directories', function(root, stat, next) {
		// Add this file to the list of files
		// console.log(stat)
		for (var i = stat.length - 1; i >= 0; i--) {
			folders.push(root + stat[i].name);
		};
		next();
	});

	walker.on('end', function() {
		// console.log(folders);
		callback(folders);
	});
}

function folderToRawData(folderPath, callback) {

	var pathSplit= folderPath.split("/");
	var mac=pathSplit[pathSplit.length-2];
	// console.log(mac);

	// console.log(folderPath);
	var files = getFilesBySize(folderPath);
	// console.log(files);

	var img4Redis = [];
	var img4RedisLength = Math.min(5, 3 + Math.floor(Math.random()*3));
	var html = '';
	var hasFirstHtmlFile = false;

	for (var j = 0; j < files.length; j++) {

		// get file extension
		var splitFilename = files[j].path.split('.');
		var ext = splitFilename[splitFilename.length-1];

		// check if extension is allowed
		for (var k = 0; k < allowedExtension.length; k++) {
			if(ext.toLowerCase() == allowedExtension[k]) {

				if(ext.toLowerCase() == 'html' && !hasFirstHtmlFile){
					html = files[j].path;
					hasFirstHtmlFile = true;
				}
				
				// get 5 biggest images over 10ko (~10000)
				else if(ext.toLowerCase() != 'html' && img4Redis.length<img4RedisLength && files[j].size>10000){

					// console.log("[files[j].path] : "+files[j].path)
					console.log('img4Redis.length: '+ img4Redis.length);
					// resizeImageSync(files[j].path);
					img4Redis.push(files[j].path);
				}
			};
		};
	};

	// format data
	var rawdata = {
		"images" : img4Redis,
		"html": html,
		"mac" : mac,
		"path" : folderPath
	}
	// console.log(JSON.stringify(rawdata))

	callback(JSON.stringify(rawdata))
}

// get files and sort them by size (biggest first)
function getFilesBySize(folderPath, callback){
	// var stats = fs.statSync(folderPath);
	var files=fs.readdirSync(folderPath)
	// console.log(files);

	var notSorted = [];
	for(var i = 0; i < files.length; i++){
		files[i] = folderPath+'/'+files[i];

		// console.log(files[i]);
		var stats = fs.lstatSync(files[i]);
		// console.log('['+i+'] '+files[i]+': '+stats.size);
		notSorted.push({'path': files[i], 'size':stats.size});
		// console.log(notSorted[i]);
	};

	sorted = notSorted.sort(compare);
	return sorted;
}

// utility function used to sort obj by an attribute
function compare(a,b) {
	if (a.size > b.size)
		return -1;
	if (a.size < b.size)
		return 1;
	return 0;
}

// resize image according to maxHeight
function resizeImageSync(_image, maxHeight) {
  // get img info
  easyimg.identify(_image, function(err, img, stderr) {
      if (err) throw err;

      var ratio;
      var options =  {
         srcData:fs.readFileSync(_image, 'binary')
      };

      console.log('Original: ' + img.width + ' x ' + img.height);

      ratio=maxHeight/img.width;

      console.log("ratio : "+ ratio); 
      options.height=maxHeight;
      options.width=ratio*img.width;

      //  resize the image
      easyimg.resize(options, function(err, stdout) {
         if (err) throw err;

        fs.writeFileSync(_image, stdout, 'binary');

      }); 
  });
}

function getTimeDiff(folderPath, callback) {
	fs.stat(folderPath, function (err, stats) {
		// TODO : fix ugly hack
		if(err) return // throw new Error("path is wrong: "+ folderPath);
		var complete = false;
		var timediff = moment().unix()-moment(stats.mtime).unix();
		// console.log(timediff);

		callback(timediff,folderPath);
	})
}

// Check if Object is inside Array
Array.prototype.hasObject = (
	!Array.indexOf ? function (o) {
		var l = this.length + 1;
		while (l -= 1){
			if (this[l - 1] === o){
				return true;
			}
		}
		return false;
	} : function (o) {
		return (this.indexOf(o) !== -1);
	}
);

function deleteFolderRecursive(folderPath) {
	if( fs.existsSync(folderPath) ) {
		fs.readdirSync(folderPath).forEach(function(file,index){
			var curPath = folderPath + "/" + file;
			if(fs.statSync(curPath).isDirectory()) { // recurse
				deleteFolderRecursive(curPath);
			} else { // delete file
				fs.unlinkSync(curPath);
			}
		});
		fs.rmdirSync(folderPath);
		console.log(folderPath + " deleted successfully !");
	}
};

// export as module
module.exports={
	"folderToRawData" : folderToRawData,
	"getTimeDiff" : getTimeDiff,
	"search4Folders" : search4Folders,
	"deleteFolderRecursive" : deleteFolderRecursive
}