var datauri = require("datauri");
var countries = require('country-data').countries;
var geoip = require('geoip-lite');
var Packet = require("./Packet").Packet;

var fs = require('fs');
var cheerio = require('cheerio');
var iconv = require('iconv-lite');
var jschardet = require("jschardet")

function parsePacket(_rawdata, callback) {
	// var raw = _rawdata;
	/*
	getText( _rawdata, function(html_text) {
		
		var packet = createPacket(_rawdata.ip, _rawdata.from, html_text, _rawdata.images);

		callback(raw,packet)
	});
	*/
	var packet=new Packet;
	
	packet.setGeo(getGeo(_rawdata.ip));
	packet.setFrom(_rawdata.from);
	packet.setMac(_rawdata.mac);

	for (var i=_rawdata.images.length - 1; i >= 0; i--) {
		var image=getImageURI(_rawdata.images[i]);
		// console.log(i+"/"+_rawdata.images.length,"image");
		packet.addImage(image);
	};

	// console.log(html_text);
	
	getText( _rawdata, function(html_text) {
		packet.setText(html_text);
	});

	callback(_rawdata,packet);
}

/////////////////////////////// PRIVATE FUNCTIONS

function getText(_rawdata, callback){

	// create a buffer from file, find encoding, return as String
	var text = '';
	var buffer=fs.readFileSync(_rawdata.html, null) ;
	var encoding = jschardet.detect(buffer).encoding;
	var html 
	try {
		html= iconv.decode(buffer, encoding);
	} catch (e) {
		return
	}

	var $ = cheerio.load(html);

	 // get page title
	var matches = html.match(/<title>(.*?)<\/title>/);

	if(matches != null) {
		if(matches.length > 1 ) {
			var pageTitle = matches[1];
			text += pageTitle;
		}
	}
	
	// get headers
	var headers = $('h1,h2,h3,h4,h5,h6');
	var headerMinLength = 10;
	var textMaxHeadersLength = 200; // if we want to limit titles
	for (var i = 0; i < headers.length; i++) {
		var t = $(headers[i]).text();
		t = t.split('\n').join(' ');
		t = t.split('\r').join(' ');
		if(t.length>headerMinLength && text.length < textMaxHeadersLength){
			text += ' ' + t;
		}
	};
	$('h1,h2,h3,h4,h5,h6').remove();
	
		

	// get text
	rawText = $('body').text();
	var rawLines = rawText.match(/[^\r\n]+/g);
	var lineMinLength = 60;
	if(rawLines!=null) {
		for (var i = 0; i < rawLines.length; i++) {
			if(rawLines[i].length>lineMinLength){
				text += ' ' + rawLines[i];
			}
		};
	}
	// remove tabs
	text = text.split('\t').join(' ');
	
	// remove multiple [space]
	var sp = text.split(' ');
	text = '';
	for (var i = 0; i < sp.length; i++) {
		if(sp[i] !== '') text += sp[i]+' ';
	};

	// substr text if longer than 400 chars
	text = text.substr(0, Math.min(400, text.length)) + '...';
	
	// get images
	var imgs = $('body').find('img');
	var imgsSrc = {};
	for (var i = 0; i < imgs.length; i++) {
		imgsSrc[i] = $(imgs[i]).attr('src');
	};

		
	callback(text);
}

function getGeo(ip) {
	// console.log(ip,geoip.lookup(ip));
	var ip_info = geoip.lookup(ip);
	var geo;

	if (ip_info.city != "") {
        geo=ip_info.city;
    } else if(countries[ip_info.country] == undefined) {
    	geo = "China";
    } else {
		geo=countries[ip_info.country].name;
    }

	return geo;
}

function getFrom(req) {
	var from;
	// if(req="GET") from=0
	//	 else from=1
	from = req;
	return from;
}

function getImageURI(path) {
	var uri = datauri(path);
	// console.log(path);
	// console.log(uri);
	return uri
}

function createPacket(ip, from, html_text, images_path){
	var packet=new Packet;
	
	packet.setGeo(getGeo(ip));
	packet.setFrom(from);

	for (var i=images_path.length - 1; i >= 0; i--) {
		var image=getImageURI(images_path[i]);
		console.log(i+"/"+images_path.length,"image");
		packet.addImage(image);
	};

	// console.log(html_text);
	packet.setText(html_text);
	
	return packet;
}

module.exports = {
	"parsePacket" : parsePacket
}