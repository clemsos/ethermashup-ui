var geoip = require('geoip-lite');
var ips = require("./ips.json")
var ranges=["china","europe","usa","hongkong"];


function getIpFamily() {
    
    var range = [0,1,2,3];
    var rand=getRandomInt(1,100)

    var rnd = Math.random();
    var r = rnd <.6 ? 0:
        rnd < .75 ? 1 :
        rnd < .95 ? 2 :
        3;

    return r
}

function getRandomIpInRange() {

    var ip;

    var country=ranges[getIpFamily()];
    var ipcountry= ips[country];
    var iprange=ipcountry[getRandomInt(0,ipcountry.length)]
    
    var range_split=iprange.split(".");
    while(range_split == undefined) {
            iprange=ipcountry[getRandomInt(0,ipcountry.length)];
             range_split=iprange.split(".");
    }
    var ip_final=range_split[3].split("/");

    console.log(ip_final);

    if(ip_final.length!=1) {
        // if(ip_final[0]<ip_final[1]) 
        ip = range_split[0]+"."+range_split[1]+"."+range_split[2]+"."+ip_final[0];
        // else {
        //     ip = range_split[0]+"."+range_split[1]+"."+range_split[2]+"."+getRandomInt(ip_final[1], ip_final[0]);
        // }
    } else {
        ip="220.181.111.86" // baidu IP by defualt!
    }

    console.log(ip);
    return ip
}

// GENERATE FAKE IP
function getRandomInt (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function isValidIp(_ip) {
    var valid;

    if(geoip.lookup(_ip) == null )  valid = false;
    else valid = true;

    return valid;
}

function getRandomIp() {
    var _ip;
    while(! isValidIp(_ip) ){
        _ip=getRandomIpInRange();
    }
    return _ip;
} 

function getRandomFrom () {
    return getRandomInt(0,1)
}

module.exports={
    "getRandomIp" : getRandomIp,
    "getRandomFrom" : getRandomFrom
}