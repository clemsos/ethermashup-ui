# Ether Mashup Server

The **Ether Mashup Server** turns data html pages and pictures from wifi sniffing into visual logs. Data sources are provided by the [Ether Mashup Devices](https://bitbucket.org/clemsos/ethermashup-devices) that collects elements by sniffing wifi networks.

The code featured here is part of **The Ether Mashup**, an art project about network surveillance by Clément Renaud, Lionel Radisson and Nicolas Maigret.

## Get started
### Spec

    server-side : Bash, NodeJS, Redis, MongoDB, ImageMagick
    client-side : Html5, jQuery, js, Sound API (Chrome only)

### How it works

* **Config** : The main config file is located in ```/config/config.json```. 
* **Data** : the server scan each folder in ETHER_DEVICE_LIST (Array) located within ETHER_TMP_DIR (dir path)
* **Queue** : All folders older than 2 min containing files bigger than 5k are queued in Redis
* **Parsing** : IPs are geocoded, images resized, html text parsed
* **Storage** : Parsed data is stored in MongoDB
* **Display** : Data is sent to client through Web Sockets

### Routes
* ```/```        : Nothing
* ```/device```  : list of available devices 
* ```/devices``` : list of available pages 

## Dev
Launch in dev mode
```
    git clone http://bitbucket.org/clemsos/ethermashup-ui
    # create a new config file and edit it
    cp /config/config.json.sample /config/config.json
    # start redis server
    redis-server
    # debug mode
    bash dev_start.sh
```

## Install

    # install nvm
    cd ~
    git clone git://github.com/creationix/nvm.git
    mv ~/nvm ~/.nvm
    . ~/.nvm/nvm.sh

    nvm install v0.8.8
    nvm use v0.8.8
    nvm alias default v0.8.8
    # npm is now bundled in node !

    sudo apt-get install nvm mongodb redis-server
    npm install supervisor -g

    npm install




## Deploy
You will need Ruby and Capistrano to deploy properly.

    # config : change ip, port, user, adminuser to match your server 
    cp config/deploy/production.rb.sample config/deploy/production.rb
    cap -T # list all tasks
    cap deploy:setup production
    cap deploy production

## Utils

##### Export mongo data to json

    mongoexport -d ethermashup -c packets --out dump/dump.json

##### Separate your mongo dump in multiple smaller json files

    node utils/jsoner.js