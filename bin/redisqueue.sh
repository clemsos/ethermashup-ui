#!/bin/bash
#
# This script scan files and add it ot redis queue

# define port
port=$ETHER_REMOTE_PORT || 9000

tmpdir=/tmp/data/
donelist=/tmp/donelist

# find folders to scan (MAC addresses)
devicelist="$(find ${tmpdir} -maxdepth 1 -not -path ${tmpdir} | awk -F"/" '{print $NF}')"
echo $devicelist


##################################################################
# SANITY CHECKS
#################################

echo "sanity checks in progress..."

which redis-cli &>/dev/null
if [ $? -ne 0 ]; then
    echo "ERROR : please install 'redis' on src before using this script"
    exit 1
fi

# check if list of already processed files
[[ -f ${donelist} ]] || touch ${donelist}

echo "sanity checks complete..."

while : ; do
    
    for id in $devicelist; do

        # define redis list name (one for each device)
        q="ethermashup:raw:${id}"

        # check for html files in folder
        if [[ ! -z `find ${tmpdir}${id} -name \*.html` ]]; then

            # scan folder for html files
            htmls=$(find ${tmpdir}${id} -name \*.html)

            for page in $htmls; do
            
                # check if the page has already been processed
                if [[ -z  `cat ${donelist} | grep ${tmpdir}${id}${page}`  ]]; then

                    echo "found new html files from device ${id}"

                    # push new files to redis list
                    redis-cli publish ${q} "${page}" ;
                    echo "${page} pushed to redis ${q}"
                    
                    # add url to list of already done 
                    echo ${tmpdir}${id}${page} >> ${donelist}
                fi
            done
        fi
    done
    echo "Waiting for files."
    # sleep $(expr 60 \* 2)
    sleep $(expr 5)

done

# listen to port, get data and add it into redis queue
# nc -l ${port} | while read x; do redis-cli rpush ${list} "$x" ; done 
